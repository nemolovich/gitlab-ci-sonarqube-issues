FROM python:3.10-alpine
LABEL maintainer="Brian GOHIER<nemolovich@hotmail.fr>"

COPY requirements.txt /root/requirements.txt

RUN pip install -r /root/requirements.txt

COPY main.py /root/main.py
COPY convert-report /bin/convert-report

RUN chmod +x /root/main.py && chmod +x /bin/convert-report

ENTRYPOINT [ "/bin/convert-report" ]
CMD [ "-h" ]
