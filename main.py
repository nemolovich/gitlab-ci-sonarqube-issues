#!/bin/python

import json
import sys
from argparse import Namespace, ArgumentParser
from multiprocessing.dummy import Pool
from multiprocessing import pool as mp
from pathlib import Path
from typing import Optional, Sequence, Any

import requests
from requests import Response

DEFAULT_MODE: str = 'pullRequest'
MODES: list[str] = ['branch', DEFAULT_MODE]
DEFAULT_SKIP_RESOLVED: bool = True
MAX_SIZE: int = 500
DEFAULT_FACETS: list[str] = []
FACETS = ["projects",
          "files",
          "assigned_to_me",
          "severities",
          "statuses",
          "resolutions",
          "rules",
          "assignees",
          "author",
          "directories",
          "scopes",
          "languages",
          "tags",
          "types",
          "pciDss-3.2",
          "pciDss-4.0",
          "owaspAsvs-4.0",
          "owaspTop10",
          "owaspTop10-2021",
          "sansTop25",
          "cwe",
          "createdAt",
          "sonarsourceSecurity",
          "codeVariants",
          "cleanCodeAttributeCategories",
          "impactSoftwareQualities",
          "impactSeverities",
          "issueStatuses"
          ]
DEFAULT_ADD_FIELDS: list[str] = ['_all']
ADD_FIELDS: list[str] = DEFAULT_ADD_FIELDS + [
    'comments',
    'languages',
    'rules',
    'ruleDescriptionContextKey',
    'transitions',
    'actions',
    'users'
]
DEFAULT_TZ: str = 'Europe/Paris'
DEFAULT_SORT: str = 'CREATION_DATE'
SORTS: list[str] = [
    DEFAULT_SORT,
    'CLOSE_DATE',
    'SEVERITY',
    'STATUS',
    'FILE_LINE',
    'HOTSPOTS',
    'UPDATE_DATE'
]


class JsonSerializable:
    def __str__(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=2)

    def __repr__(self):
        return self.__str__()


class JsonResponsePaging(JsonSerializable):
    total: int
    pageSize: int
    pageIndex: int


class JsonResponseTextRange(JsonSerializable):
    startLine: int
    endLine: int


class JsonResponseIssues(JsonSerializable):
    key: str
    component: str
    rule: str
    severity: str
    message: str
    type: str
    hash: str
    textRange: JsonResponseTextRange


class JsonResponseComponent(JsonSerializable):
    key: str
    path: str


class JsonResponseRule(JsonSerializable):
    key: str
    name: str


class JsonResponse(JsonSerializable):
    total: int
    paging: JsonResponsePaging
    issues: list[JsonResponseIssues]
    components: list[JsonResponseComponent]
    rules: list[JsonResponseRule]

    def __init__(self, dict_):
        self.__dict__.update(dict_)


class CiQualityLines(JsonSerializable):
    begin: int
    """The start line on which the code quality violation occurred."""
    end: int
    """The end line on which the code quality violation occurred."""


class CiQualityLocation(JsonSerializable):
    path: str
    """The relative path to the file containing the code quality violation."""
    lines: CiQualityLines


class CiQuality(JsonSerializable):
    """
        Gitlab CI quality JSOn format
        See https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool
    """
    description: str
    """A description of the code quality violation."""
    check_name: str
    """A unique name representing the static analysis check that emitted this issue."""
    fingerprint: str
    """A unique fingerprint to identify the code quality violation. For example, an MD5 hash."""
    severity: str
    """A severity string (can be info, minor, major, critical, or blocker)."""
    location: CiQualityLocation


class ProgramArgs(Namespace):
    sonar_url: str
    sonar_token: str
    project_key: str
    mode: str
    dest_file: str
    mode_value: str
    verbose: bool
    show_resolved: bool
    sort_field: str
    size: int
    facets: str
    add_field: list[str]
    tz: str


class ProgramArgsParser(ArgumentParser):

    def __int__(self, *args, **kwargs):
        super(ArgumentParser, self).__init__(*args, **kwargs)

    def parse_args(self, args: Optional[Sequence[str]] = ...) -> ProgramArgs:
        n: Namespace = super().parse_args()
        # noinspection PyTypeChecker
        return n


def call_request(url: str, params: dict[str:Any], headers: dict[str, str], verbose: bool = False) -> Response:
    if verbose:
        print(f'Call {url} with parameters {params}')
    return requests.get(url, params=params, headers=headers)


def get_request(args: dict[str, Any], idx: int) -> dict[str, Any]:
    new_params: dict[str, Any] = args.copy()
    new_params['p'] = idx
    return new_params


def convert_response(response: Response, verbose: bool) -> JsonResponse:
    if response.status_code != 200:
        print(f'Cannot get result from {req_url} with parameters {req_params}', file=sys.stderr)
        sys.exit(1)
    json_dict: object = response.json()
    if verbose:
        print(f'response: "{json_dict}"')
    json_response: JsonResponse = json.loads(json.dumps(json_dict), object_hook=JsonResponse)
    return json_response


def on_success(prev_resp: JsonResponse, responses: list[Response], verbose: bool = False) -> None:
    for rsp in responses:
        rsp_json: JsonResponse = convert_response(rsp, verbose)
        prev_resp.issues += rsp_json.issues
        prev_resp.rules += rsp_json.rules
        prev_resp.components += rsp_json.components
        prev_resp.total += rsp_json.total


def create_ci_result(json_resp: JsonResponse) -> list[CiQuality]:
    result: list[CiQuality] = []
    for issue in json_resp.issues:
        ciq: CiQuality = CiQuality()
        ciq.fingerprint = issue.key
        ciq.check_name = issue.hash
        comp: JsonResponseComponent = next(filter(lambda c: c.key == issue.component, json_resp.components), None)
        if comp is None:
            raise Exception(f'Cannot find component for key {issue.component}')
        rule: JsonResponseRule = next(filter(lambda r: r.key == issue.rule, json_resp.rules), None)
        if rule is None:
            raise Exception(f'Cannot find rule for key {issue.rule}')
        ciq.description = rule.name + '.\n' + issue.message
        ciq.severity = issue.severity.lower()
        ciq.location = CiQualityLocation()
        ciq.location.path = comp.path
        ciq.location.lines = CiQualityLines()
        ciq.location.lines.begin = issue.textRange.startLine
        ciq.location.lines.end = issue.textRange.endLine
        result += [ciq]
    return result


if __name__ == '__main__':
    parser: ProgramArgsParser = ProgramArgsParser(
        prog='gitlab-ci-sonarqube-issues',
        description='Converts the SonarQube server\'s Issues to Gitlab CI format.'
                    'It requests the SonarQube server via the WEB API to obtain the results of the issues and '
                    'convert them to the expected format '
                    '(see https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool).',
        epilog='Gitlab repo: https://gitlab.com/nemolovich/gitlab-ci-sonarqube-issues.  \n\n'
               'Docker image: https://hub.docker.com/repository/docker/nemolovich/gitlab-ci-sonarqube-issues')
    parser.add_argument('sonar_url', help='the SonarQube server URL (eg. http://localhost:9000)')
    parser.add_argument('sonar_token', help='the Sonar token to use to request API')
    parser.add_argument('project_key', help='the Sonar project key to request')
    parser.add_argument('mode_value', help='the branch name or pull request number')
    parser.add_argument('dest_file', help='the file in which to save result')
    parser.add_argument('-a', '--add-field', default=DEFAULT_ADD_FIELDS, choices=ADD_FIELDS, nargs='*',
                        help=f'add a field to retrieve in response (default {DEFAULT_ADD_FIELDS})')
    parser.add_argument('-f', '--facets', default=DEFAULT_FACETS, choices=FACETS, nargs='*',
                        help=f'add a field to retrieve in response (default {DEFAULT_FACETS})')
    parser.add_argument('-m', '--mode', default=DEFAULT_MODE, choices=MODES,
                        help=f'select the request mode between Branch and Pull Request (default {DEFAULT_MODE})')
    parser.add_argument('-o', '--sort-field', default=DEFAULT_SORT, choices=SORTS,
                        help=f'select the issues sort field (default {DEFAULT_SORT})')
    parser.add_argument('-r', '--show-resolved', default=False,
                        help=f'define if resolved issues must be shown (default false)', action='store_true')
    parser.add_argument('-s', '--size', default=MAX_SIZE, help=f'max elements by request (default {MAX_SIZE})',
                        type=int)
    parser.add_argument('-t', '--time-zone', default=DEFAULT_TZ, help=f'set the timezone (default {DEFAULT_TZ})',
                        dest='tz')
    parser.add_argument('-v', '--verbose', action='store_true', help='enable verbose mode')
    prog_args: ProgramArgs = parser.parse_args()

    req_url: str = f'{prog_args.sonar_url}/api/issues/search'
    req_params: dict = {
        prog_args.mode: prog_args.mode_value,
        'components': prog_args.project_key,
        's': prog_args.sort_field,
        'ps': prog_args.size,
        'facets': prog_args.facets,
        'additionalFields': ','.join(prog_args.add_field),
        'timeZone': prog_args.tz,
    }
    if not prog_args.show_resolved:
        req_params['resolved']: str = 'false'
    req_headers: dict[str:str] = {'Authorization': f'Bearer {prog_args.sonar_token}'}
    resp: Response = call_request(req_url, req_params, req_headers, prog_args.verbose)
    json_obj: JsonResponse = convert_response(resp, prog_args.verbose)

    ps: int = json_obj.paging.pageSize
    total: int = json_obj.paging.total
    if ps < total:
        total_idx: int = total // ps + ((total % ps + (ps - 1)) // ps)
        if prog_args.verbose:
            print(f'need other requests ({ps}/{total} = {total_idx} calls)')
        with Pool(10) as pool:
            calls: mp.AsyncResult = pool.map_async(
                lambda x: call_request(req_url, get_request(req_params, x), req_headers),
                range(2, total_idx + 1),
                callback=lambda r: on_success(json_obj, r, verbose=prog_args.verbose))
            calls.wait()
    ci_result: list[CiQuality] = create_ci_result(json_obj)
    dest_path: Path = Path(prog_args.dest_file)
    if not dest_path.parent.exists():
        dest_path.parent.mkdir(parents=True, exist_ok=True)
    with dest_path.open(mode='w', encoding='UTF-8') as f:
        f.write(ci_result.__str__())
