# gitlab-ci-sonarqube-issues

Converts the SonarQube server's Issues to Gitlab CI format.

It requests the SonarQube server via the WEB API to obtain
the issues of a project for a particular branch or Pull
Request and then converts them to the format expected by
Gitlab CI quality.

Gitlab CI Quality custom tool documentation:
https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool

SonarQube WEB API issues search documentation:
https://next.sonarqube.com/sonarqube/web_api/api/issues/search

Docker tags: https://hub.docker.com/repository/docker/nemolovich/gitlab-ci-sonarqube-issues/tags?page=1&ordering=last_updated


## Example usage in Gitlab CI

To use in Merge Request, modify your **.gitlab-ci.yaml** file as follow :

```yaml

# Sonar Analysis
stages:
  # - [...]
  - quality
  - post-quality
  # - [...]

your_sonar_step:
  # [...]
  stage: quality
  # [...]
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

# Sonarqube issues converter
code quality:
  image: nemolovich/gitlab-ci-sonarqube-issues:0.0.4
  stage: post-quality
  needs:
    - your_sonar_step
  # CI_MERGE_REQUEST_IID => MR global identifier for the pullRequest mode
  script:
    - convert-report ${SONAR_HOST_URL} ${SONAR_TOKEN} ${SONAR_PROJECT_KEY} ${CI_MERGE_REQUEST_IID} report.json -v
  allow_failure: true
  artifacts:
    reports:
      codequality: report.json
    paths: [ report.json ]
    expire_in: 1 week
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```


## Command line options

```text
usage: gitlab-ci-sonarqube-issues [-h]
                                  [-a [{_all,comments,languages,rules,ruleDescriptionContextKey,transitions,actions,users} ...]]
                                  [-f [{projects,files,assigned_to_me,severities,statuses,resolutions,rules,assignees,author,directories,scopes,languages,tags,types,pciDss-3.2,pciDss-4.0,owaspAsvs-4.0,owaspTop10,owaspTop10-2021,sansTop25,cwe,createdAt,sonarsourceSecurity,codeVariants,cleanCodeAttributeCategories,impactSoftwareQualities,impactSeverities,issueStatuses} ...]]
                                  [-m {branch,pullRequest}]
                                  [-o {CREATION_DATE,CLOSE_DATE,SEVERITY,STATUS,FILE_LINE,HOTSPOTS,UPDATE_DATE}]
                                  [-r] [-s SIZE] [-t TZ] [-v]
                                  sonar_url sonar_token project_key mode_value
                                  dest_file

Converts the SonarQube server's Issues to Gitlab CI format.It requests the
SonarQube server via the WEB API to obtain the results of the issues and
convert them to the expected format (see
https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool).

positional arguments:
  sonar_url             the SonarQube server URL (eg. http://localhost:9000)
  sonar_token           the Sonar token to use to request API
  project_key           the Sonar project key to request
  mode_value            the branch name or pull request number
  dest_file             the file in which to save result

options:
  -h, --help            show this help message and exit
  -a [{_all,comments,languages,rules,ruleDescriptionContextKey,transitions,actions,users} ...], --add-field [{_all,comments,languages,rules,ruleDescriptionContextKey,transitions,actions,users} ...]
                        add a field to retrieve in response (default ['_all'])
  -f [{projects,files,assigned_to_me,severities,statuses,resolutions,rules,assignees,author,directories,scopes,languages,tags,types,pciDss-3.2,pciDss-4.0,owaspAsvs-4.0,owaspTop10,owaspTop10-2021,sansTop25,cwe,createdAt,sonarsourceSecurity,codeVariants,cleanCodeAttributeCategories,impactSoftwareQualities,impactSeverities,issueStatuses} ...], --facets [{projects,files,assigned_to_me,severities,statuses,resolutions,rules,assignees,author,directories,scopes,languages,tags,types,pciDss-3.2,pciDss-4.0,owaspAsvs-4.0,owaspTop10,owaspTop10-2021,sansTop25,cwe,createdAt,sonarsourceSecurity,codeVariants,cleanCodeAttributeCategories,impactSoftwareQualities,impactSeverities,issueStatuses} ...]
                        add a field to retrieve in response (default [])
  -m {branch,pullRequest}, --mode {branch,pullRequest}
                        select the request mode between Branch and Pull
                        Request (default pullRequest)
  -o {CREATION_DATE,CLOSE_DATE,SEVERITY,STATUS,FILE_LINE,HOTSPOTS,UPDATE_DATE}, --sort-field {CREATION_DATE,CLOSE_DATE,SEVERITY,STATUS,FILE_LINE,HOTSPOTS,UPDATE_DATE}
                        select the issues sort field (default CREATION_DATE)
  -r, --show-resolved   define if resolved issues must be shown (default
                        false)
  -s SIZE, --size SIZE  max elements by request (default 500)
  -t TZ, --time-zone TZ
                        set the timezone (default Europe/Paris)
  -v, --verbose         enable verbose mode
```
